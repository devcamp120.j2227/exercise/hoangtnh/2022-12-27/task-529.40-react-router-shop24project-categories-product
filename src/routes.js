import Categories from "./Components/bodyComponent/bodyContent/Categories";
import HomePageShop24h from "./pages/homePage";


const routerList = [
    {path: "/", element: <HomePageShop24h/>},
    {path:"/Categories", element:<Categories/>}
]
export default routerList;