import { Col, Row,Container, Breadcrumb, BreadcrumbItem } from "reactstrap";
import '@fortawesome/fontawesome-free/css/all.min.css';
import { useNavigate, useParams } from "react-router-dom";
import LoginModal from "../modalComponent/loginModal";
const Header = () =>{
    const navigate = useNavigate();
    const onLogoClick = () =>{
        navigate("/");
    }
    const onBtnProductClick = () =>{
        navigate("/Categories");
    }
    return (
        <>
        <div style={{backgroundColor:"#d1c286"}}>
            <Container >
                <Row style={{padding:"10px", backgroundColor:"#d1c286"}}>
                    <Col sm={3} className="d-flex align-items-center justify-content-center">
                        <h3 style={{color:"white"}} type="button" onClick={onLogoClick}>Devcamp COSY-HOME</h3>
                    </Col>
                    <Col sm={6} className="d-flex align-items-center justify-content-center" style={{padding:"0px"}}>
                        <Row sm={12} style={{margin:"0px"}}>
                            <Col sm={3} className = "header-content" >
                                <a type="button" style={{padding:"5px"}} onClick={onBtnProductClick}>PRODUCT</a>
                            </Col>
                            <Col sm={3} className = "header-content" >
                                <a type="button"style={{padding:"5px"}} >ABOUT</a>
                            </Col>
                            <Col sm={3} className = "header-content" >
                                <a type="button" style={{padding:"5px"}} >SERVICES</a>
                            </Col>
                            <Col sm={3} className = "header-content" >
                                <a type="button"style={{padding:"5px"}} >CONTACT</a>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={3} className="d-flex align-items-center justify-content-end">
                        <Col sm={4}>
                            <p style={{margin:"0px"}}><i type="button" className="fa-solid fa-magnifying-glass"></i> Search</p>
                        </Col>
                        <Col sm={4}>
                            <p style={{margin:"0px"}}><i type="button" className="fa-solid fa-cart-shopping"></i> Cart</p>
                        </Col>
                        <Col sm={4}>
                            <LoginModal/>
                        </Col>
                    </Col>
                </Row> 
            </Container>
        </div>
        </>
    )
}
export default Header;