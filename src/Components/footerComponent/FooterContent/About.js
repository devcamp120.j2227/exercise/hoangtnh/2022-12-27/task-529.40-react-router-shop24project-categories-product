import { Row } from "reactstrap"
import '@fortawesome/fontawesome-free/css/all.min.css';
const About =() => {
    return (
        <>
            <div className="d-flex align-items-start flex-column">
                <Row className="mb-4">
                    <h4>About Shop</h4>
                </Row>
                <Row>
                    <p><i className="fa-solid fa-location-dot"></i> Address</p>
                </Row>
                <Row>
                    <p><i className="fa-solid fa-mobile-screen-button"></i> +84901414197</p>
                </Row>
                <Row>
                    <p><i className="fa-regular fa-envelope"></i> homedecor@gmail.com</p>
                </Row>
            </div>
        </>
    )
}
export default About;