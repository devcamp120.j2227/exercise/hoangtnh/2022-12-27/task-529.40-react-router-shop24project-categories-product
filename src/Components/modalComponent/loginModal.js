import { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Label, Input, FormGroup } from 'reactstrap';
import '@fortawesome/fontawesome-free/css/all.min.css';


function LoginModal(args) {
  const [open, setOpen] = useState(false);

  const toggle = () => setOpen(!open);

  return (
    <div>
        <i className="fa-solid fa-user" onClick={toggle}></i>
      <Modal isOpen={open} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle} style={{color:"#d1c286"}}>Sign In</ModalHeader>
            <ModalBody >
                <Row >
                    <Col className='d-flex justify-content-center' sm={12}>
                        <Button color='danger' style={{ width:"80%",borderRadius:"50px"}}>
                        <i className="fa-brands fa-google"/> Sign In with <b>Google</b> 
                        </Button>
                    </Col>
                    <Col className='d-flex justify-content-center' sm={12}>
                    <div><hr style={{width:"150px",border:"1px solid black"}}/></div>
                    </Col>
                    <Col sm={12} className='d-flex flex-column align-items-center'>
                        <FormGroup style={{width:"80%"}} sm={12}>
                            <Input
                            name="username"
                            placeholder="Username"
                            style={{borderRadius:"50px"}}
                            />
                        </FormGroup>
                        <FormGroup style={{width:"80%"}}>
                            <Input
                            name="password"
                            placeholder="Password"
                            type="password"
                            style={{borderRadius:"50px"}}
                            />
                        </FormGroup>
                        <Button color='success' style={{ width:"80%",borderRadius:"50px"}}>
                            Sign In  
                        </Button>
                    </Col>
                </Row>
            </ModalBody>
        <ModalFooter>
            <Col className='text-center'>
                <p>Don't have an account?  <p  style={{color:"green"}}>Sign up here</p></p>
            </Col>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default LoginModal;