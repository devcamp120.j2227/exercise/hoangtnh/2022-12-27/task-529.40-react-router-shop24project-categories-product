import "bootstrap/dist/css/bootstrap.min.css"
import "./App.css";
import Content from "./views/Content";
import ContentBodyShop24h from "./views/Content";
import FooterShop24h from "./views/Footer";
import HeaderShop24h from "./views/Header";

function App() {
  return (
    <div >
        <HeaderShop24h/>
        <Content/>
        <FooterShop24h/>
    </div>
  );
}

export default App;
